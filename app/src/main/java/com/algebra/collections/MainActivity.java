package com.algebra.collections;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class MainActivity extends AppCompatActivity {

    private EditText etInput;
    private EditText etNumbers;
    private Button bList;
    private Button bSet;
    private Button bTest;
    private Button bTask;
    private Button bAdd;
    private TextView tvResult;

    private List<String> stringList;
    private Set<String> stringSet;

    private List<Integer> numberList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        stringList = new ArrayList<>();
        //stringSet = new HashSet<>();
        stringSet = new TreeSet<>();

        numberList = new ArrayList<>();
        initWidgets();
        setupListeners();
    }

    private void initWidgets() {
        etInput = findViewById(R.id.etInput);
        etNumbers = findViewById(R.id.etTask);
        bList = findViewById(R.id.bList);
        bSet = findViewById(R.id.bSet);
        bTest = findViewById(R.id.bTest);
        bAdd = findViewById(R.id.bAdd);
        bTask = findViewById(R.id.button4);
        tvResult = findViewById(R.id.tvResult);
    }

    private void setupListeners() {
        bList.setOnClickListener(view -> {
            String input = getInput();
            stringList.add(input);
        });

        bSet.setOnClickListener(view -> {
            String input = getInput();
            stringSet.add(input);
        });

        bTest.setOnClickListener(view -> {
            printText(stringList.toString());
            printText("size: " + stringList.size());
            printText("first element:" + stringList.get(0));
            printText("last element:" + stringList.get(stringList.size() - 1));
            printText("contains test: " + stringList.contains("test"));
            printText("isEmpty: " + stringList.isEmpty());
            stringList.remove("abc");
            printText(stringList.toString());
            Collections.sort(stringList);
            printText("After sort: " + stringList.toString());
            Collections.shuffle(stringList);
            printText("After shuffle: " + stringList.toString());

            for (int i = 0; i < stringList.size(); i++) {
                printText(i + ":" + stringList.get(i));
            }

            printText("--------");

            for (String element : stringList) {
                printText(element);
            }

            printText(stringSet.toString());
            printText("size: " + stringSet.size());
            printText("contains test: " + stringSet.contains("test"));
            printText("isEmpty: " + stringSet.isEmpty());
            stringSet.remove("abc");
            printText(stringSet.toString());
        });

        bAdd.setOnClickListener(view -> {
            numberList.add(Integer.valueOf(etNumbers.getText().toString()));
        });

        bTask.setOnClickListener(view -> {
            List<String> names = new ArrayList<>();
            names.add("Ivan");
            names.add("Ana");
            names.add("Marko");
            names.add("Marija");

            printText(names.get(0));
            printText(names.get(names.size() - 1));

            Collections.sort(names);
            printText(names.toString());


            Set<Integer> numbersSet = new TreeSet<>();
            numbersSet.add(3);
            numbersSet.add(19);
            numbersSet.add(92);
            numbersSet.add(-5);

            for (int num : numbersSet) {
                printText(num + "");
            }

            Collections.sort(numberList);

            int minNumber = numberList.get(0);
            int maxNumber = numberList.get(numberList.size() - 1);
            int total = 0;

            for (int number : numberList) {
                total = total + number;
            }
            double average = total / (double) numberList.size();

            String result = "Min number: " + minNumber + "\n" + "Max number: " + maxNumber + " \n" + "Average: " + average;
            Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
        });
    }

    private String getInput() {
        return etInput.getText().toString();
    }

    private void printText(String text) {
        tvResult.append(text + "\n");
    }
}
